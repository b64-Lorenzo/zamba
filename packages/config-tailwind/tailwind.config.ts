import type { Config } from 'tailwindcss';
import flowbite from 'flowbite/plugin';
import * as path from "path";

// This is a very hacky way to get the proper base path for the library,
// as we need to pass it to Tailwind to scan for Tailwind classes. Normally,
// we would "just" pass a reference to node_modules, but since we're using
// Yarn PnP that doesn't work.
const flowbitePath = path.dirname(require.resolve("flowbite/plugin"));

export default {
    darkMode: 'class',
    content: {
        relative: true,
        files: [
            "./src/**/*.ts",
            "./index.html",
            "./src/**/*.{js,ts,jsx,tsx}",
            `${flowbitePath}/**/*.js`,
            "../../packages/ui/**/*.{js,ts,jsx,tsx}"
        ],
    },
    theme: {
        fontFamily: {
            sans: ['Inter', 'sans-serif']
        },
        extend: {
            colors: {
                "fmf": {
                    //DEFAULT: "#09529e",
                    DEFAULT: "#173494",
                    'dark': "#173494",
                }
            },
        },
    },
    plugins: [
        flowbite,
    ],
} satisfies Config;