# `@byma/ui` Component Library

The components, primarily designed for Lorenzo Zambelli's websites, are both accessible and customizable. Some of these components are also available in `@fmfnl/ui`.

## What's inside?

This repo includes the following packages and apps:

### Apps and Packages

- `apps/docs`: Component documentation site with Storybook
- `packages/ui`: Core Lit & React components  with [Tailwind CSS](https://tailwindcss.com/)
- `packages/utils`: Shared Lit & React utilities
- `packages/typescript-config`: Shared `tsconfig.json`s
- `packages/eslint-config`: ESLint preset

Each package and app is 100% [TypeScript](https://www.typescriptlang.org/).

## License

MIT License

```bash
corepack enable
corepack prepare yarn@stable --activate
```