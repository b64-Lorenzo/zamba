/** @type {import("eslint").Linter.Config} */
module.exports = {
  root: true,
  extends: ["@byma/eslint-config/index.js"],
};
