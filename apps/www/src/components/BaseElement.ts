import { LitElement } from "lit";

export default abstract class BaseElement extends LitElement {
    protected createRenderRoot(): HTMLElement {
        return this;
    }
}
