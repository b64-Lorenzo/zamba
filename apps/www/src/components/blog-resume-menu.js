
import React, { useState } from 'react';
import Blog from './blog'
import Resume from './resume';

const Menu = () => {
    const [selectedItem, setSelectedItem] = useState('resume');
  
    const handleItemClick = (item) => {
      setSelectedItem(item);
    };
  
    return (
      <div>
        <div class="p-7 pb-0 block-section bg-white rounded-xl">
          <ul className="flex space-x-8 font-medium">
            <li>
              <a
                className={selectedItem === 'resume' ? 'menu-link-active menu-link-hover' : 'menu-link'}
                onClick={() => handleItemClick('resume')}
              >
                Resume
              </a>
            </li>
            <li>
              <a
                className={selectedItem === 'blog' ? 'menu-link-active menu-link-hover' : 'menu-link'}
                onClick={() => handleItemClick('blog')}
              >
                Blog
              </a>
            </li>
          </ul>
        </div>
        <br />
        
        {/* Conditionally render components based on the selected item */}
        {selectedItem === 'resume' ? <Resume /> : <Blog />}
      </div>
    );
  };
  
  export default Menu;